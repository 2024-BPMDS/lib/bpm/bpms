package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.task;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("dev-bpms")
@Service
@AllArgsConstructor
public class BpmsTaskServiceDev extends BpmsTaskService{

}
