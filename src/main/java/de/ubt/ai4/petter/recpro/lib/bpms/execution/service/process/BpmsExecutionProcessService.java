package de.ubt.ai4.petter.recpro.lib.bpms.execution.service.process;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public abstract class BpmsExecutionProcessService {

    public void startProcess(String processId) {

    }

    public void setAttribute(String attributeId, String processInstanceId, Object value) {

    }

    public void setAttributes(String processInstanceId, Map<String, Object> attributes) {}
}
