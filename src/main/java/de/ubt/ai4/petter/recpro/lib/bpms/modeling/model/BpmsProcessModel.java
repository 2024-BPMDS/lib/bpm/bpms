package de.ubt.ai4.petter.recpro.lib.bpms.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsProcessModel {
    private String id;
    private String xml;
    private List<BpmsActivity> activities = new ArrayList<>();
    private List<BpmsRole> roles = new ArrayList<>();
}
