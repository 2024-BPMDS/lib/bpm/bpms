package de.ubt.ai4.petter.recpro.lib.bpms.execution.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsProcessInstance {
    private String id;
    private String definitionId;
    private String businessKey;
    private String caseInstanceId;
}
